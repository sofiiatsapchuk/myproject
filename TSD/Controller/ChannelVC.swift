//
//  ChannelVC.swift
//  TSD
//
//  Created by Sofiia Tsapchuk on 7/16/18.
//  Copyright © 2018 Sofiia. All rights reserved.
//

import UIKit

class ChannelVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60
    }
}
